from functools import wraps
from pprint import pprint


def slice_lines(string, slice_, ellipsis=None):
    lines = string.split('\n')
    start = slice_.start or 0
    stop = slice_.stop or len(lines)
    assert start >= 0 and stop >=0
    ellipsis = ellipsis is None and '\n'.join(['...'] * 2) or ellipsis

    out = []
    if start > 0:
        out.append(ellipsis)
    out.extend(lines[start:stop])
    if stop < len(lines):
        out.append(ellipsis)
    return '\n'.join(out)


def head(string, lines=5):
    return slice_lines(string, slice(0, lines))


def print_this(f):
    return print_function_or_method(f)


def print_function_or_method(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        name = f.__name__
        cls = getattr(f, 'im_class', '')
        if cls:
            name = '.'.join((cls.__name__, name))
        print 'entering:'
        print '\t%s(*%r, **%r)' % (name, args, kwargs,)
        try:
            result = f(*args, **kwargs)
            print 'leaving:'
            print '\t%s(*%r, **%r)' % (name, args, kwargs,)
            print '\tresult:'
            if isinstance(result, basestring):
                print head(result)
            else:
                pprint(result)
            return result
        except Exception as e:
            print 'exception:'
            print '\tfunction or method: %s(*%r, **%r)' % (name, args, kwargs,)
            print '\t%r' % (e,)
            raise e
    return wrapper
